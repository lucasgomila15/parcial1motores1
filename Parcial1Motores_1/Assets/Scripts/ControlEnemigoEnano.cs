using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlEnemigoEnano : MonoBehaviour
{ 
    public CapsuleCollider colliderSuperior;

    void Update()
    {
        RaycastHit hit;

        if (Physics.Raycast(transform.position, transform.up, out hit, 0.7f))
        {
            transform.gameObject.SetActive(false);
        }
    }
}
