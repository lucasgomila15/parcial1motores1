using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlCamara : MonoBehaviour
{
    public float velocidadRotacion = 2f;

    void Update()
    {
        // Obt�n los movimientos del rat�n en el eje horizontal y vertical
        float mouseX = Input.GetAxis("Mouse X") * velocidadRotacion;
        float mouseY = Input.GetAxis("Mouse Y") * velocidadRotacion;

        // Rota la c�mara en funci�n de los movimientos del rat�n
        transform.Rotate(Vector3.up, mouseX);
        // Puedes limitar la rotaci�n vertical si lo deseas
        transform.Rotate(Vector3.left, mouseY);
    }
}
