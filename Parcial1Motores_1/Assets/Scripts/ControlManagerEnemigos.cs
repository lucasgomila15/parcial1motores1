using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlManagerEnemigos : MonoBehaviour
{
    public GameObject jugador;
    public int rapidez;
    public float distanciaMinima = 20f;
    public float X;
    public float Y;
    public float Z;

    void Update()
    {
        Vector3 direccionAlJugador = jugador.transform.position - transform.position;

        direccionAlJugador.y = 0f;

        float distanciaAlJugador = direccionAlJugador.magnitude;

        if (distanciaAlJugador <= distanciaMinima)
        {
            direccionAlJugador.Normalize();
            transform.Translate(direccionAlJugador * rapidez * Time.deltaTime, Space.World);
        }
        if (distanciaAlJugador > distanciaMinima)
        {
            transform.LookAt(gameObject.transform);
            gameObject.transform.position = new Vector3(X, Y, Z);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Fondo"))
        {
            gameObject.SetActive(false);
        }
    }
}



