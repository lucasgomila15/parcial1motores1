using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlEnemigoCambiante : MonoBehaviour
{
    public float amplitud = 3f;
    public float velocidad = 2f;
    private Vector3 escalaInicial;

    private void Start()
    {
        escalaInicial = transform.localScale;
    }

    private void Update()
    {
        float cambioEscala = Mathf.Sin(Time.time * velocidad) * amplitud;

        float nuevaEscalaX = escalaInicial.x + cambioEscala;
        float nuevaEscalaZ = escalaInicial.z + cambioEscala;

        transform.localScale = new Vector3(nuevaEscalaX, 1f, nuevaEscalaZ);
    }
}
