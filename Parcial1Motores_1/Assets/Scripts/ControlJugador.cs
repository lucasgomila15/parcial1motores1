using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlJugador : MonoBehaviour
{
    public Text textoGanaste;
    public Text textoCantidadUvas;
    public Text textoPerdiste;
    public Text textoCronometro;

    private int puntosUva;
    private int puntoFinal;
    private float tiempoRestante;

    private void Start()
    {
        puntosUva = 0;
        puntoFinal = 0;
        setearTextos();
        comenzarJuego();
    }

    void Update()
    {
        if (puntosUva >= 5 && puntoFinal == 1)
        {
            StartCoroutine(comenzarCronometro(0));
            textoGanaste.text = "YOU WIN!!!";
        }
    }

    private void setearTextos()
    {
        textoCantidadUvas.text = "Uvas Coleccionadas: " + puntosUva.ToString();
    }

    private void comenzarJuego()
    {
        StartCoroutine(comenzarCronometro(60));
    }

    public IEnumerator comenzarCronometro(float valorCronometro = 60)
    {
        tiempoRestante = valorCronometro;

        while (tiempoRestante > 0)
        {
            textoCronometro.text = tiempoRestante.ToString();
            yield return new WaitForSeconds(1.0f);
            tiempoRestante--;
        }

        if (tiempoRestante <= 0)
        {
            transform.gameObject.SetActive(false);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemigo"))
        {
            gameObject.SetActive(false);
            textoPerdiste.text = "GAME OVER";
        }
        if (collision.gameObject.CompareTag("Bala"))
        {
            gameObject.SetActive(false);
            textoPerdiste.text = "GAME OVER";
        }
        if (collision.gameObject.CompareTag("Uva"))
        {
            puntosUva += 1;
            setearTextos();
        }
        if (collision.gameObject.CompareTag("Final"))
        {
            puntoFinal += 1;
        }
    }
}
