using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlEnemigoVolador : MonoBehaviour
{
    public GameObject jugador;
    public int rapidez;
    public float distanciaMinima = 20f;
    public float X;
    public float Y;
    public float Z;

    void Update()
    {
        Vector3 direccionAlJugador = jugador.transform.position - transform.position;

        direccionAlJugador.y = 0f;

        float distanciaAlJugador = direccionAlJugador.magnitude;

        if (distanciaAlJugador <= distanciaMinima)
        {
            direccionAlJugador.Normalize();

            transform.LookAt(jugador.transform);
            transform.Rotate(0f, 0f, 0f);
            transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
        }

        if(distanciaAlJugador > distanciaMinima)
        {
            transform.LookAt(gameObject.transform);
            gameObject.transform.position = new Vector3(X, Y, Z);
        }
    }
}
