using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlBloqueMovedorEspejo : MonoBehaviour
{
    bool bajar = false;
    float rapidez = 3f;
    public float alturaMinima;
    public float alturaMaxima;

    private void Update()
    {
        if (transform.position.x >= alturaMaxima)
        //0.80
        {
            bajar = true;
        }
        if (transform.position.x <= alturaMinima)
        //0.42
        {
            bajar = false;
        }

        if (bajar)
        {
            Bajar();
        }
        else
        {
            Subir();
        }
    }

    private void Bajar()
    {
        transform.position -= transform.right * rapidez * Time.deltaTime;
    }

    private void Subir()
    {
        transform.position += transform.right * rapidez * Time.deltaTime;
    }
}
