using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlBarraVertical : MonoBehaviour
{
    bool bajar = false;
    public float rapidez;
    public float alturaMinima;
    public float alturaMaxima;

    private void Update()
    {
        if (transform.position.y >= alturaMaxima)
        {
            bajar = false;
        }
        if (transform.position.y <= alturaMinima)
        {
            bajar = true;
        }

        if (bajar)
        {
            Bajar();
        }
        else
        {
            Subir();
        }
    }

    private void Bajar()
    {
        transform.position += transform.up * rapidez * Time.deltaTime;
    }

    private void Subir()
    {
        transform.position -= transform.up * rapidez * Time.deltaTime;
    }
}
