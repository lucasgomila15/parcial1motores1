using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlEnemigoDisparador : MonoBehaviour
{
    public Transform objetivo; 
    public GameObject balaPrefab; 
    public Transform puntoDisparo;
    public float velocidadBala = 10f;
    public float distanciaDisparo = 10f;

    private float tiempoUltimoDisparo;

    private void Update()
    {
        float distanciaAlObjetivo = Vector3.Distance(transform.position, objetivo.position);

        if (distanciaAlObjetivo <= distanciaDisparo && Time.time - tiempoUltimoDisparo >= 1f)
        {
            Disparar();
            tiempoUltimoDisparo = Time.time;
        }

        Vector3 direccion = objetivo.position - transform.position;
        transform.rotation = Quaternion.LookRotation(direccion.normalized);
    }

    private void Disparar()
    {
        GameObject bala = Instantiate(balaPrefab, puntoDisparo.position, transform.rotation);
        Rigidbody rb = bala.GetComponent<Rigidbody>();
        rb.velocity = transform.forward * velocidadBala;
        Destroy(bala, 2f);
    }
}
