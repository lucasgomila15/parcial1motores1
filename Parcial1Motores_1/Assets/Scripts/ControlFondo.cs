using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlFondo : MonoBehaviour
{
    public GameObject jugador;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            jugador.transform.position = new Vector3(0f, 1f, 0f);
        }
    }
}
