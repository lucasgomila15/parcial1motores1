using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlPuntoFinal : MonoBehaviour
{
    private void Update()
    {
        transform.Rotate(new Vector3(15, 30, 0f) * Time.deltaTime);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            gameObject.SetActive(false);
        }
    }
}