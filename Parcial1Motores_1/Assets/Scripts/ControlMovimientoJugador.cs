using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlMovimientoJugador : MonoBehaviour
{
    private float velocidad = 5f;
    private int saltosRestantes = 2;

    private float escalaContadorX;
    private float escalaContadorY;
    private float escalaContadorZ;

    private void Update()
    {
        float movimientoHorizontal = 0f;
        float movimientoVertical = 0f;
        float fuerzaSalto = 6;

        if (Input.GetKey(KeyCode.W))
        {
            movimientoVertical = 1f;
        }
        if (Input.GetKey(KeyCode.S))
        {
            movimientoVertical = -1f;
        }
        if (Input.GetKey(KeyCode.A))
        {
            movimientoHorizontal = -1f;
        }
        if (Input.GetKey(KeyCode.D))
        {
            movimientoHorizontal = 1f;
        }
        if (Input.GetKey(KeyCode.R))
        {
            gameObject.transform.position = new Vector3(0f, 1f, 0f);
        }
        if (Input.GetKeyDown(KeyCode.Space) && (saltosRestantes > 0))
        {
            GetComponent<Rigidbody>().AddForce(Vector3.up * fuerzaSalto, ForceMode.Impulse);
            saltosRestantes--;
        }
        Vector3 movimiento = new Vector3(movimientoHorizontal, 0f, movimientoVertical) * velocidad * Time.deltaTime;
        transform.Translate(movimiento);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Piso"))
        {
            saltosRestantes = 2;
        }

        if (collision.gameObject.CompareTag("Bull"))
        {
            velocidad += 3f;
            escalaContadorX += 0.35f;
            escalaContadorY += 0.25f;
            escalaContadorZ += 0.35f;
            transform.localScale = new Vector3(1f + escalaContadorX, 1f + escalaContadorY, 1f + escalaContadorZ);
        }
        if (collision.gameObject.CompareTag("Transformador"))
        {
            velocidad -= 0.7f;
        }
    }
}
