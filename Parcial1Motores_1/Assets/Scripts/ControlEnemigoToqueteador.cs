using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlEnemigoToqueteador : MonoBehaviour
{
    public GameObject jugador;
    private int cantidadToques = 2;
    public Text textoPerdiste;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Rigidbody playerRb = collision.gameObject.GetComponent<Rigidbody>();
            playerRb.AddForce(transform.forward * 500f);

            cantidadToques--;

            if (cantidadToques == 0)
            {
                jugador.gameObject.SetActive(false);
                textoPerdiste.text = "GAME OVER";
            }
        }
    }
}
