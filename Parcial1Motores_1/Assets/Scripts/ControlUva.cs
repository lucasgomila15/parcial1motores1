using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlUva : MonoBehaviour
{
    public GameObject jugador;

    bool uvaBajar = false;
    float rapidez = 0.5f;
    public float alturaMinima;
    public float alturaMaxima;

    private void Update()
    {
        if(transform.position.y >= alturaMaxima) 
        {
            uvaBajar = false;
        }
        if (transform.position.y <= alturaMinima)
        {
            uvaBajar = true;
        }

        if (uvaBajar)
        {
            Bajar();
        } else
        {
            Subir();
        }
    }

    private void Bajar()
    {
        transform.position += transform.up * rapidez * Time.deltaTime;
    }

    private void Subir()
    {
        transform.position -= transform.up * rapidez * Time.deltaTime;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            gameObject.SetActive(false);
        }
    }
}
